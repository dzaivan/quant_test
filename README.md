Quantox Symfony Test
========================

**Tasks** You will have to run tasks from the command line to generate statistic: 

* **RabbitMQ consumer**: 
php bin/console rabbitmq:consumer -w save_request_data

* **Cron job (frequency depends on traffic)**: php bin/console app:calculate-statistic
