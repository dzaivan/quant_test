<?php
/**
 * Created by PhpStorm.
 * Date: 5/29/2018
 * Time: 4:13 PM
 */

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends FOSRestController
{
    /**
     * @Rest\Post("/api")
     */
    public function indexAction(Request $request)
    {
        $requestedParams = $request->request->all();

        $requestedParams['eventDate'] = new \DateTime();

        $this->get('old_sound_rabbit_mq.save_request_data_producer')->publish(serialize($requestedParams));

        $data = ['status' => 'ok'];

        $view = $this->view($data,
            Response::HTTP_CREATED);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/api")
     */
    public function getStatisticAction()
    {
        $cacheKey = 'statisticEventData';

        $cachedItem = $this->get('cache.app')->getItem($cacheKey);

        if (false === $cachedItem->isHit()) {
            $statisticData = array();
        } else {
            $statisticData = unserialize($cachedItem->get());
        }

        $view = $this->view($statisticData,
            Response::HTTP_CREATED);
        return $this->handleView($view);
    }
}