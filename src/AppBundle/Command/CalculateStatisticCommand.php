<?php
/**
 * Created by PhpStorm.
 * Date: 5/30/2018
 * Time: 4:26 PM
 */

namespace AppBundle\Command;

use AppBundle\Entity\EventLog;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalculateStatisticCommand extends Command
{
    private $entityManager;

    private $cache;

    public function __construct(EntityManagerInterface $entityManager, CacheItemPoolInterface $cache)
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:calculate-statistic')
            ->setDescription('Calculates statistic.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $countries = $this->entityManager->getRepository('AppBundle:EventLog')->getTopCountries();

        $countries = $this->formatCountriesArray($countries);

        $statisticData = $this->entityManager->getRepository('AppBundle:EventLog')
            ->getStatisticForCountries($countries);

        $statisticData = $this->formatStatisticData($statisticData);

        $cacheKey = 'statisticEventData';

        $cachedItem = $this->cache->getItem($cacheKey);
        $cachedItem->set(serialize($statisticData));
        $this->cache->save($cachedItem);

    }

    private function formatCountriesArray($countries)
    {
        $onlyCountryCodes = [];

        foreach($countries as $countryTmp) {
            $onlyCountryCodes[] = $countryTmp['countryCode'];
        }

        return $onlyCountryCodes;
    }

    private function formatStatisticData($statisticData)
    {
        $resStatisticData = [];

        foreach($statisticData as $staticItem) {
            $eventType = EventLog::$eventTypes[$staticItem['eventType']];

            $resStatisticData[$staticItem['countryCode']][] = [ 'eventType'     => $eventType,
                'eventTotal'    => $staticItem['eventTotal']
            ];
        }

        return $resStatisticData;
    }

}