<?php
/**
 * Created by PhpStorm.
 * Date: 5/29/2018
 * Time: 9:05 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="EventLogRepository")
 * @ORM\Table(name="event_log", indexes={@ORM\Index(name="search_idx", columns={"event_date", "country_code", "event_type"})})
 */
class EventLog
{
    public static $eventTypes = [ 1 => 'view', 2 => 'click', 3 => 'play'];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $eventDate;

    /**
     * @ORM\Column(type="string")
     */
    private $countryCode;

    /**
     * @ORM\Column(type="integer")
     */
    private $eventType;

    /**
     * @ORM\Column(type="integer")
     */
    private $counter = 0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * @param mixed $eventDate
     * @return EventLog
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     * @return EventLog
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param mixed $eventType
     * @return EventLog
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * @param mixed $counter
     * @return EventLog
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;
        return $this;
    }

    public function increaseCounter()
    {
        $this->counter = $this->counter + 1;
    }

}