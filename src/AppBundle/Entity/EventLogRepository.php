<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class EventLogRepository extends EntityRepository
{
    public function updateCounter($params)
    {
        $eventType = array_search($params['eventType'], EventLog::$eventTypes);

        $eventLog = $this->findOneBy(
            array(  'countryCode'   => $params['countryCode'],
                    'eventDate'     => $params['eventDate'],
                    'eventType'     => $eventType )
        );

        if (empty($eventLog)) {
            $eventLog = new EventLog();
            $eventLog->setCountryCode($params['countryCode'])
                ->setEventDate($params['eventDate'])
                ->setEventType($eventType);
        }

        $eventLog->increaseCounter();

        $this->_em->persist($eventLog);
        $this->_em->flush();
    }

    public function getTopCountries()
    {
        $topCountriesLimit = 5;

        $q = $this->createQueryBuilder('e');
        $q->select('e.countryCode, SUM(e.counter) as totalEvents')
            ->groupBy('e.countryCode')
            ->orderBy('totalEvents', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults($topCountriesLimit);

        $res = $q->getQuery()->getResult();

        return $res;
    }

    public function getStatisticForCountries($countries)
    {
        $statisticLimitDays = 7;

        $endDate = new \DateTime($statisticLimitDays . ' days ago');

        $endDate = $endDate->format("Y-m-d");

        $q = $this->createQueryBuilder('e');

        $q->select('SUM(e.counter) as eventTotal, e.countryCode, e.eventType')
            ->where('e.countryCode IN (:countries)')
            ->andWhere("e.eventDate >= '$endDate'")
            ->groupBy('e.countryCode, e.eventType');

        $q->setParameter('countries', $countries);

        $res = $q->getQuery()->getResult();

        return $res;
    }
}
