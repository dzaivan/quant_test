<?php
/**
 * Created by PhpStorm.
 * User: zorica
 * Date: 5/29/2018
 * Time: 7:37 PM
 */

namespace AppBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class SaveRequestData implements ConsumerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function execute(AMQPMessage $msg)
    {
        $requestParamsSerialized = $msg->body;
        $requestParams = unserialize($requestParamsSerialized);

        $this->entityManager->getRepository('AppBundle:EventLog')->updateCounter($requestParams);
    }

}